import React from 'react';
import store from '../stores/appStore';
import {AppConsumer} from '../Provider'
import {FavButton} from '../components/Utils/Buttons/FavButton'
import {CartButton} from '../components/Utils/Buttons/CartButton'

export const FavButtonContainers = ({id}) => (    
	<AppConsumer>
        {({ actions, state }) => (
            <FavButton active={state.favourites.map[id]}  
                onActivate={()=>actions.addFavourite(id)} 
                onDeactivate={()=>actions.removeFavourite(id)}  />
        )}
    </AppConsumer>
)
export const CartButtonContainers = ({id, className}) => (
    <AppConsumer>
            {({  actions, state  }) => { 
                    //  console.log('button',state)         
                    //  console.log('id',state.cart.map[id])         
                return(
                    <CartButton in_cart={state.cart.map[id]|| false} className = {className || "btn btn-block"}
                        onAdd={()=>actions.addToCart(id)} 
                        onRemove={()=>actions.removeFromCart(id)}  />
                    )
                }
            }
    </AppConsumer>
)