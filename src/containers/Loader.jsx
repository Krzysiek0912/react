import React from 'react'
import {AppConsumer} from '../Provider'


class LoaderContainer extends React.Component {
    render() {
      return (
        <AppConsumer>
             {({state}) => {
              //  console.log(state.isLoading);
               return(          
                state.isLoading? <div className="loader"></div> : (this.props.children || null)
            
         )}
         }
        </AppConsumer>
      );
    }
}
export default LoaderContainer