import React from 'react'
import {ShoppingCartList} from '../components/Lists/ShoppingCartList';
import {FavouritesCoursesList} from '../components/Lists/FavouritesCoursesList';
import {CoursesList} from '../components/Lists/CoursesList';
import {CoursesSingle} from '../components/Lists/CursesSingle'
import {CoursesEditor} from '../components/Course/Editor';

import {AppConsumer} from '../Provider'
const projector = (selectors, projector) => {	
	return(
	
	(state) => {
		return(								
			projector.apply(null, selectors.map(selector => selector(state)) )
		)
		}
	)
}
const getCoursesList = (listSelector) => projector([
	listSelector,
	state => state.entities.courses
], (list, entities) =>{
		// console.log('list i entitis',list,entities);
	return(
		
		{
			list: list.map(id => entities[id])
		}
	)
})

export class CoursesListContainer extends React.Component {
	  render() {	    
	    return (
	      <AppConsumer>
	   		{({state,actions}) => {
					//  let test = (state) => state.courses.paged_list
					//  console.log('test', test(state));
					 let list = state.courses.paged_list.map((id)=> state.entities.courses[id])
					//  let list = getCoursesList(state.courses.paged_list);
					 return(
					 <React.Fragment>
	        <CoursesList list={list} props={this.props} state={state}/> 
				  <hr />
					<button className="btn btn-default btn-block" onClick={actions.loadMore}> Pokaż więcej ... </button>
					</React.Fragment>
				 
				)}}
				   </AppConsumer>
					 
				
	    );
	  }
}
export class CourseContainer extends React.Component {
	  render() {	

	    
	    return (
	      <AppConsumer>
	   		{({state}) => {
					 let list = state.courses.paged_list.map((id)=> state.entities.courses[id])
					 return(
	        <CoursesSingle list={list} props={this.props}/> 
				 )}
				}
	      </AppConsumer>
	    );
	  }
}
export class FavouritesCoursesListContainer extends React.Component {
	  render() {	  	
	    return (
	      <AppConsumer>
	   		{({state}) => {
					let list = state.favourites.list.map((id)=> state.entities.courses[id])
				 return (
	        <FavouritesCoursesList list={list} props={this.props} state={state}/> 
				 )}
				}
	      </AppConsumer>
	    );
	  }
}
export class ShoppingCartListContainer extends React.Component {
	  render() {
	    return (
	      <AppConsumer>
				{({state}) => {
					let list = state.cart.list.map((id)=> state.entities.courses[id])
				 return (
	        <ShoppingCartList list={list}/> 
				 )}
				}
	      </AppConsumer>
	    );
	  }
}

export class CoursesEditorContainer extends React.Component {
	

	render() {
		return (
			<AppConsumer>
					 {({state, actions}) => { 
						let courses = state.courses.list.map((id)=> state.entities.courses[id])
						let results = state.search_results.list.map((id)=> state.entities.courses[id])
					return(
					<CoursesEditor actions={actions} courses={courses} results={results}
									selected={state.search_results.selected}
									revisions={state.revisions.courses}
					></CoursesEditor>
			 )}
			 }
			</AppConsumer>
		);
	}
}