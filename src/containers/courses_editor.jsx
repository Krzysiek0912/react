import React from 'react'
import {CoursesEditor} from '../components/Course/Editor';

import {AppConsumer} from '../Provider'


export class CoursesEditorContainer extends React.Component {
    render() {
      return (
        <AppConsumer>
             {({state, actions}) => { 
              let results = state.search_results.list.map((id)=> state.entities.courses[id])
              let selected = state.search_results.selected.map((id)=> state.entities.courses[id])
              let courses = state.courses.map((id)=> state.entities.courses[id])
               return(
          <CoursesEditor actions={actions} courses={courses} results={results}
                    selected={selected}
                    revisions={revisions}/>
         )}
         }
        </AppConsumer>
      );
    }
}