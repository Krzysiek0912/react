import createStore from './createStore';

import ACTIONS from '../constants/actions'


const  dataStore = createStore({
	entities:{
		courses:{},
		authors:{},
		categories:{}
	}

}, function(action) {
	let payload = action.payload;
	let entities = this.state.entities;
	
	switch(action.type){

		case ACTIONS.LOAD_COURSES:

			payload.forEach(course => {
				entities.courses[course.id] = entities.courses[course.id] || course;

				entities.authors[course.author.id] = entities.authors[course.author.id] || course.author
				course.categories.forEach(category => {
					entities.categories[category] = entities.categories[category] || category;
				})
			})

			this.emitChange();
			break;

		case ACTIONS.SAVE_COURSE:

			var course = payload.course;
			var id = course.id;

			entities.courses[id] = course;
			entities.authors[course.author] = course.author;

			course.categories.forEach(category => {
				entities.categories[category] = entities.categories[category] || category;
			})

			this.emitChange();
			break;
	}

})


export default dataStore;