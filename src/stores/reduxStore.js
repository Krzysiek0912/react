import {createStore, combineReducers} from 'redux';
import ACTIONS from '../constants/actions';
import createListReducer from './createListReducer'


import DevTools from '../DevTools'
const initialState = {

}

const entitiesReducer = (state={
	courses:{}
}, action)=>{

	switch(action.type){

		case ACTIONS.LOAD_COURSES:

			let courses = action.payload.reduce( (map, course) => {
				map[course.id] = course;
				return map;
			},{})

			return {
				...state,
				courses,
			};

		case ACTIONS.SAVE_COURSE:

			var course = action.payload.course;
			var id = course.id;

			return {
				...state,
				courses: { ...state.courses, [id]:course }
			}

		default:
	  		return state;
	}
}

const loadingReducer = (state=false,action) =>{

	switch(action.type){
		case ACTIONS.START_LOADING:
			return true;

		case ACTIONS.STOP_LOADING:

			return false;

		default:
	  		return state;
	}
}

const configReducer = (state={
	labels:{
		add_fav: "Dodaj do Ulubionych",
		remove_fav: "Usuń z Ulubionych",
	}
},action) => {
	return state
}

const revisionsReducer = (state={
	courses:{}
},action) => {
	switch(action.type){

		case ACTIONS.SAVE_COURSE:
			var course = action.payload.course;

			var revision = {
				timestamp: Date.now(),
				revision: { ...course, categories:[...course.categories ]}
			}
			var revisions = state.courses[course.id];
			return {
				...state,
				courses: { ...state.courses, [course.id]: [ ...(revisions||[]), revision ]}
			}

		default: 
			return state;
	}
}



const authorsReducer = (state={
	list:[],
	map:{}
}, action)=>{

	switch(action.type){

		case ACTIONS.LOAD_COURSES:
			var map = action.payload.reduce( (map, course) => {
				map[course.authorId] = course.author;
				return map;
			},{})
			var list = Object.keys(map).map( id => map[id] )

			return {...state,
				list: list,
				map: map
			};
		default: 
			return state;
	}
}

const rootReducer = combineReducers({
	entities: entitiesReducer,
	isLoading: loadingReducer,
	config: configReducer,

	revisions: revisionsReducer,

	courses: createListReducer({ name:'COURSES',
		actions:{
			LOAD: ACTIONS.LOAD_COURSES,
			LOAD_MORE: ACTIONS.LOAD_MORE_COURSES
		}}),

	authors: authorsReducer,

	favourites: createListReducer({
		name:'FAVOURITES',
		actions:{
			LOAD: ACTIONS.LOAD_FAVOURITES,
			ADD: ACTIONS.ADD_TO_FAVOURITES,
			REMOVE: ACTIONS.REMOVE_FROM_FAVOURITES
		}
	}),
	cart: createListReducer({
		name:'CART',
		actions:{
			LOAD: ACTIONS.LOAD_CART,
			ADD: ACTIONS.ADD_TO_CART,
			REMOVE: ACTIONS.REMOVE_FROM_CART
		}
	}),
	search_results: createListReducer({
		name:'SEARCH_RESULTS',
		actions:{
			LOAD: ACTIONS.LOAD_SEARCH_RESULTS,
			LOAD_MORE: 'LOAD_MORE_SEARCH_RESULTS',
			SELECT: ACTIONS.SELECT_IN_SEARCH_RESULTS
		}
	})
})

const store = createStore(rootReducer, initialState, DevTools.instrument())

export default store;