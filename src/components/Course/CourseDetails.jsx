import React from 'react';
import { Rating } from './Rating'
import { CartButtonContainers } from '../../containers/buttons'

export class CourseDetails extends React.Component {
	
	constructor(props) {
		super(props);
		this.data = props.data;
		this.props = props;

	}
	// shouldComponentUpdate = (nextProps, nextState) => {
	// 	console.log('nextstatedetails',nextProps)
	// 	if (nextProps.data != this.props)
	// 		return true
	// 	return false
	// }
	render() {
		return (
			<div>
				<table className="table course_details">
					<tbody>
						<tr>
							<th>Ocena</th>
							<td>
								<Rating max={5} value={this.data.rating} />
							</td>
						</tr>
						<tr>
							<th>Autor</th>
							<td>{this.data.author.name}</td>
						</tr>
						<tr>
							<th>Czas trwania</th>
							<td>{this.data.duration}</td>
						</tr>
						<tr>
							<th>Cena</th>
							<td>{this.data.price} PLN</td>
						</tr>
					</tbody>
				</table>
				<CartButtonContainers id={this.data.id} />
			</div>
		)
	}
}
export default CourseDetails;