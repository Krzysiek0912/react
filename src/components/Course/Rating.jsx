import React from 'react';

export class Rating extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			indicator: this._makeIndicator(this.props.value, this.props.max),
			rating: this.props.value,
		};

	}
	onMouseEnter(i){
		return () => this.setIndicator(i);
	}
	onMouseLeave(i){
		return () => this.setIndicator(this.state.rating);
	}
	onClick(i){
		return () => this.setRating(i);
	}
	
	setRating(rating){
		this.setState({
			rating: rating
		});
		this.setIndicator(rating);
		this.props.onChange(rating);
	}
	setIndicator(rating){
		this.setState({
			indicator: this._makeIndicator(rating, this.props.max)
		})
	}

	_makeIndicator(rating, max){
		return [ ...Array(rating).fill(true), ...Array(max-rating).fill(false) ]
	}

	render(){
		return <div>
			{this.state.indicator.map((item, i) => (<span key={i} 
				className={"glyphicon " + (item? "glyphicon-star" : "glyphicon-star-empty")}
				onMouseEnter={this.onMouseEnter(i+1)} 
				onMouseLeave={this.onMouseLeave(i+1)}
				onClick={this.onClick(i+1)}></span>)) }
		</div>
	}
}

