import React from 'react';
// import { AppConsumer } from '../../../Provider'
import { AppConsumer } from '../../../Provider'

import { CourseCategoriesEditor } from './CourseCategoriesEditor'
import { AuthorsSelector } from './AuthorsSelector'
export class CourseForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			authors:{ ...this.props.authors },
			course: { ...this.props.course }

		};
	}
	componentWillReceiveProps(nextProps) {
		this.setState({ course: { ...nextProps.course } });
	}

	changedTitle = (e) => {
		this.setState({
			course: { ...this.state.course, title: e.target.value }
		})

	}

	changedDescription = (e) => {
		this.setState({
			course: { ...this.state.course, description: e.target.value }
		})
	}

	changedAuthor = (author) => {
		this.setState({
			course: { ...this.state.course, author: author }
		})
	}

	changedIsPromo = (e) => {
		this.setState({
			course: { ...this.state.course, is_promo: !this.state.course.is_promo }
		})
	}

	changedIsNew = (e) => {
		this.setState({
			course: { ...this.state.course, is_new: !this.state.course.is_new }
		})
	}

	changedCategories = (categories) => {

		this.setState({
			course: { ...this.state.course, categories: categories }
		})
	}

	onSave = (event) => {
		event.preventDefault();
		this.setState({
			course: { ...this.state.course, ...this.state.course }
		})
		this.props.onSave(this.state.course)
	}

	render() {
		return <div>
			<form onSubmit={this.onSave}>
				<div className="form-group">
					<label className="control-label">Nazwa Kursu:</label>
					<div>
						<input type="text" className="form-control" value={this.state.course.title} onChange={this.changedTitle} />
					</div>
				</div>
				<div className="form-group">
					<label className="control-label">Opis Kursu:</label>
					<div>
						<textarea rows="10" type="text" className="form-control" value={this.state.course.description}
							onChange={this.changedDescription}></textarea>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-6">
						<div className="form-group">
							<label className="control-label">Opcje:</label>
						</div>
						<div className="form-group">
							<label className="control-label col-xs-6">
								<input type="checkbox" checked={this.state.course.is_promo || ''} onChange={this.changedIsPromo} /> W promocji
								</label>


							<label className="control-label col-xs-6">
								<input type="checkbox" checked={this.state.course.is_new || ''} onChange={this.changedIsNew} /> Nowość
								</label>
						</div>
					</div>
					<div className="col-xs-6">
						<div className="form-group">
							<label className="control-label">Autor:</label>
							<AppConsumer>
							{({ state }) => (
							<AuthorsSelector authors_list={state.authors.list} authors_map={state.authors.map} value={this.state.course.author.id} onChange={this.changedAuthor} />
							)}
							</AppConsumer>
						</div>
					</div>
					<div className="col-xs-6">
						<div className="form-group">
							<label className="control-label">Kategorie:</label>
							<div>
								<CourseCategoriesEditor
									onChange={this.changedCategories}
									categories={this.state.course.categories}></CourseCategoriesEditor>
							</div>
						</div>
					</div>
				</div>
				<div className="form-group">
					<div className="btn-group pull-right">
						<input type="button" className="btn btn-danger" value="Anuluj" onClick={this.props.onCancel} />
						<input type="submit" className="btn btn-success" value="Zapisz" />
					</div>
				</div>
			</form>
		</div>
	}
}