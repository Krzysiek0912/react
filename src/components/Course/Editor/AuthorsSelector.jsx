import React from 'react';

export const AuthorsSelector = ({ authors_list, authors_map, value, onChange }) => (<div>
	<select className="form-control" value={value} onChange={(e) => onChange(authors_map[parseInt(e.target.value)])}>
		{authors_list.map((author) => <option key={author.id} value={author.id}>{author.name}</option>)}
	</select>
</div>
)

