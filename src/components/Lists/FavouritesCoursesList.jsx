import React from 'react';
import { Course, CoursePromoLabel } from '../Course';
import CourseDetails from '../Course/CourseDetails';
import Button from '../Utils/Buttons/Button'
import { FavButtonContainers } from '../../containers/buttons';
import Loader from '../../containers/Loader'
import { Link } from "react-router-dom";

export const FavouritesCoursesList = ({ list }) => {

	return (

		<div>
			<h1> Ulubione Kursy </h1>

			<hr />
			<div>
				<Loader>
					{list.length == 0 ? <p className="text-center">Brak kursów </p> : 
					list.map((data) => {
						if (data) {
							return (<Course data={data} key={data.id} Details={CourseDetails}>
								{/* Promotion */}
								<CoursePromoLabel data={data} />

								{/* Course Actions */}
								<div className="btn-group pull-right">
									<Link to={`/kurs/${data.id}`}><Button label="Szczególy kursu" /></Link>
									<FavButtonContainers id={data.id} />
								</div>
							</Course>)
						} 
					}
					)}
				</Loader>
			</div>
		</div>



	)
}