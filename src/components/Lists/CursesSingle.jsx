import React from 'react';
import {Draggable} from '../Utils/DragNDrop'
import {Course, CoursePromoLabel} from '../Course';
import Button from '../Utils/Buttons/Button'
import {FavButtonContainers} from '../../containers/buttons'
export const CoursesSingle = ({list, props}) => {
   
    let id = props.match.params.id
    function getSingle(value) {
     return value.id == id;
   }
   
   var filtered = list.filter(getSingle);
   
             
   
   return(
       <div>
           <h1> Kurs </h1>
           <hr />
           <div>
               { 	
                   
                   
                   
                   filtered.map((data) => (
                       
                       <Draggable key={data.id} data={data} image={data.image}>
                       <Course data={data} >
                             {/* Promotion */}
                             <CoursePromoLabel data={data} />
   
                             {/* Course Actions */}
                           <div className="btn-group pull-right">
                               <Button onClick={props.history.goBack} label="Powrót" />
                               
                               <FavButtonContainers id={data.id}/>
                           </div>
                       </Course>
                       </Draggable>
   
                   )
               )}
           </div>  
       </div>
   )
   }