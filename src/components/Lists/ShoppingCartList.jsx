import React from 'react';

import Button from '../Utils/Buttons/Button'
import { Course } from '../Course'
import CartDetails from '../Cart/CartDetails'
import { Link } from "react-router-dom";
import { FavButtonContainers } from '../../containers/buttons'
import Loader from '../../containers/Loader'

export const ShoppingCartList = ({ list }) => (
	<div>
		<h1> Koszyk </h1>
		<hr />
		<Loader>
			<div>
				{list.length == 0 ? <p className="text-center">Brak kursów </p> :
					list.map((data) => (<Course data={data} key={data.id} Details={CartDetails}>
						<div className="btn-group pull-right">
							<Link to={`/kurs/${data.id}`}><Button label="Szczególy kursu" /></Link>
							<FavButtonContainers id={data.id} />
						</div>
						<div><b>Autor: </b> {data.author.id} <br /> <b>Czas trwania: </b> {data.duration} </div>
					</Course>)

					)
				}
			</div>
		</Loader>
	</div>
)
