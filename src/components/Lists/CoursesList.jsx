import React from 'react';
import {Draggable} from '../Utils/DragNDrop'
import {Course, CoursePromoLabel} from '../Course';
import CourseDetails from '../Course/CourseDetails';
import Button from '../Utils/Buttons/Button'
import {FavButtonContainers} from '../../containers/buttons'
import Loader from '../../containers/Loader'

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

if (module.hot) {
  module.hot.accept();
}

export const CoursesList = ({list,props}) => {	

let match=props.match



return(
	<div>
		<h1> Kursy </h1>
		<hr />
		<Loader>
    	<div>
			{list.map((data) => <Draggable key={data.id} data={data} image={data.image}>
					<Course data={data} Details={CourseDetails}>
				  		{/* Promotion */}
			  			<CoursePromoLabel data={data} />

				  		{/* Course Actions */}
						<div className="btn-group pull-right">
							<Link to={`${match.path}/${data.id}`}><Button label="Szczególy kursu" /></Link>
							<FavButtonContainers id={data.id}/>
						</div>
					</Course>
				</Draggable>
			)}
		</div>
		</Loader>

		
		
	</div>
)
}
