import React from 'react';
import { Droppable } from '../Utils/DragNDrop'
import {AppConsumer} from '../../Provider'
import {Link } from "react-router-dom";


export const NavBar = (props) => {

	return <nav className="navbar navbar-default">
	  <div className="container-fluid">
	    <div className="navbar-header">
	      <Link to='' className="navbar-brand">EduKursy</Link>
	    </div> 

    	<ul className="nav navbar-nav navbar-left">
    		<li name="kursy">
	    		<Link to='/kurs'>Kursy</Link>
    		</li>
    		<li name="ulubione">
	    		<Link to='/fav' >Ulubione</Link>
    		</li>
    	</ul>

    	<ul className="nav navbar-nav navbar-right">
    		<li name="wyszukiwarka">
	    		<Link to='/search' >Wyszukiwarka</Link>
    		</li>
    		<li name="koszyk">
	    		<Link to='/shop'>
	    		<AppConsumer>
    				{({ actions, state }) => (

		        	<Droppable onDrop={(data)=>actions.addToCart(data)}>
	    				<span className="glyphicon glyphicon-shopping-cart"></span> Koszyk {state.cart.list.length}
		        	</Droppable>
		        )}
  				</AppConsumer>
	    		</Link>
    		</li>
    	</ul>
      </div>
  	</nav>
}