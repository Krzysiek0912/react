import React from 'react';
import { CartButtonContainers} from '../../containers/buttons'


const CartDetails = (props) => {
	return(		
	<div className="course_details text-center">	
		<h1 className="thumbnail">{props.data.price} PLN</h1>
		<CartButtonContainers  id={props.data.id}/>
	</div>
)}
export default CartDetails
