import React from 'react';
import Button from './Button'


export class CartButton extends React.Component {

	constructor(props) {
		super(props);
		this.className = props.className || "btn btn-block";
		this.state = { in_cart: this.props.in_cart };
	}
	
	addToCart = () => {
		this.setState({
			in_cart: true
		})
		this.props.onAdd()
	}
	removeFromCart = () => {
		this.setState({
			in_cart: false
		})
		this.props.onRemove()
	}
	render() {

		return (this.state.in_cart ?
			<Button className={this.className + " btn-danger"} onClick={this.removeFromCart} icon={"remove"} label={"Usuń z koszyka"} /> :
			<Button className={this.className + " btn-success"} onClick={this.addToCart} icon={"shopping-cart"} label={"Dodaj do koszyka"} />

		)
	}
}