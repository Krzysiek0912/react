import React from 'react';
import Button from './Button'

export class FavButton extends React.Component{

	constructor(props) {
	    super(props);
	    this.state = {active: this.props.active};
	}

	// static getDerivedStateFromProps(nextProps){
	// 	return {
	// 		active: nextProps.active
	// 	}
	// }

	setActive=()=>{
		this.setState({
			active: true
		})
		this.props.onActivate()
	}

	setInactive=()=>{
		this.setState({
			active: false
		})
		this.props.onDeactivate()
	}

	render(){
		
		return (this.state.active?
			<Button label="Usuń z ulubionych" icon="star" onClick={this.setInactive} /> :
			<Button label="Dodaj do ulubionych" icon="star-empty" onClick={this.setActive} />
		)
	}
}
