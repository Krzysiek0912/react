import React from 'react';

export const Draggable = (props) => {

	function onDragStart(e) {
		if (props.image) {
			let img = new Image();
			img.src = props.image;
			e.dataTransfer.setDragImage(img, 10, 10)
		}


		e.dataTransfer.setData('application/x-edukursy-kurs', props.data.id)
	}

	return <div draggable="true" onDragStart={onDragStart}>{props.children}</div>
}
export class Droppable extends React.Component {
	constructor(props) {
		super(props)
		this.props = props;
	}

	onDragOver = (e) => {
		e.preventDefault()
	}

	onDrop = (e) => {
		let data = e.dataTransfer.getData('application/x-edukursy-kurs');
		this.props.onDrop(data, e);
		// this.setState({
		// 	categories: categories
		// }
	}
	render() {
		return (
			<div onDragOver={this.onDragOver} onDrop={this.onDrop}>{this.props.children}</div>
		);
	}
}
// export const Droppable = (props) => {
// 	console.log('Droppable',props);
// 	function onDragOver(e){
// 		e.preventDefault()
// 	}

// 	function onDrop(e){
// 		let data = e.dataTransfer.getData('application/x-edukursy-kurs');
// 		props.onDrop(data, e);
// 	}

// 	return <div onDragOver={onDragOver} onDrop={onDrop}>{props.children}</div>
// }
