
import React from 'react';
import ReactDOM from 'react-dom';


import store from './stores/appStore';


import './style.css';
import './vendor/typeahead.bundle.css';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.min.css';


import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import { AppProvider } from './Provider';
import { Layout } from './components/Layout'
import {CoursesListContainer, ShoppingCartListContainer,
		FavouritesCoursesListContainer, 
		CoursesEditorContainer, CourseContainer} from './containers/courses_lists';

import actionCreators from './actions/action.creators'

import ACTIONS from './constants/actions';

import {dispatcher, dispatch} from './appDispatcher';
// dispatcher.register(function(action) {
// 	store.dispatch(action);
// })
// import dataStore from './stores/dataStore'
// dataStore.dispatchToken = dispatcher.register(function(action) {
// 	dataStore.dispatch(action)
// })
// import logStore from './stores/logStore'
// dispatcher.register(function(action) {
// 	logStore.dispatch(action)
// })
// import revisionStore from './stores/revisionStore'
// dispatcher.register(function(action) {
// 	revisionStore.dispatch(action)
// })
import reduxStore from './stores/reduxStore';

// console.log('reduxStore:', reduxStore)
dispatcher.register(function(action) {
	reduxStore.dispatch(action)
})

reduxStore.subscribe(()=>{
	// console.log('reduxStore state:', reduxStore.getState())
})

let actions = actionCreators(dispatch);


actions.fetchCourses();

actions.fetchCart();
actions.fetchFavourites();




// import courses_data from './courses_data';
// dispatch({
// 	type: ACTIONS.LOAD_COURSES,
// 	payload: courses_data,
// 	meta:{
// 		timestamp: Date.now()
// 	}
// })
import DevTools from './DevTools'
ReactDOM.render(
	<AppProvider store={reduxStore} actions={actions}>
	<DevTools store={reduxStore} />
	<Router>
		<Layout>
				<Route exact path="/" render={() => (<Redirect to="/kurs"/>)}/>
				<Route exact path="/kurs" component={CoursesListContainer}/>
				<Route path="/kurs/:id" component={CourseContainer}/>
				{/*<Route path="/#/"  component={CoursesListContainer}/>*/}
				<Route path="/shop" component={ShoppingCartListContainer} />
				<Route path="/search" component={CoursesEditorContainer} />
				<Route path="/fav" component={FavouritesCoursesListContainer} />
				{/*<Route path="*"  component={CoursesListContainer}/>*/}
		</Layout>
	</Router>
	</AppProvider>,
	document.getElementById('root'));

// if (module.hot) {
// 		module.hot.accept('./main.js', function() {
// 	     console.log('Accepting the updated printMe module!');
// 		    printMe();
// 		  })
// }