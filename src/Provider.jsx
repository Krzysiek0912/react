import React, { Component }  from 'react';
// import actions from './actions';
import AppStore from './stores/appStore';
// import AppState from './AppState';


const AppContext = React.createContext();

export class AppProvider extends Component {
  constructor(props) {
    super(props);
    this.state = AppStore.getState(),
    this.actions = props.actions
  } 

  componentDidMount(){
   this.props.store.subscribe(() => {
      this.setState(this.props.store.getState())
    });
  }
  render() {
    const { children } = this.props;

    return (
      <AppContext.Provider
        value={{
          actions: this.actions,
          state: this.state
        }}
      >{children}</AppContext.Provider>
    );
  }
}

export const AppConsumer = AppContext.Consumer;