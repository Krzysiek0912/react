var path = require('path')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
	mode: 'development',
	
	optimization: {
		minimize: false,
	 },
	entry:[
		'webpack-dev-server/client?http://localhost:8080',

		path.join(__dirname, '/src/main.jsx')
	],

	output:{
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/'
	},
	// devServer: {
 //      contentBase: './dist',
 //      hot: true
    // },
	resolve: {
    extensions: ['.js', '.jsx'],
 	},
	plugins: [
	    new ExtractTextPlugin("style.css"),
	    new HtmlWebpackPlugin({
	    	template: 'src/index.html',
	    	inject: 'body',
	    	filename: 'index.html'
	    })
	],
	// resolve: {
 //      enforceExtension: true
 // 	 }, 	
	module: {
		rules: [
		    {
		      test: /\.m?jsx?$/,
		      exclude: /(node_modules|bower_components)/,
		      use: {
		        loader: 'babel-loader',
		       //  options: {
		       //  	presets: ["@babel/preset-env", "@babel/preset-react"]
		      	// }
		        
		      }
		    },
		 	{
			    test: /\/App\.jsx?$/, // regex to match files to receive react-hot-loader functionality
			    loader: require.resolve('react-hot-loader-loader'),
			},
		    {
		        test: /\.css$/,
		        use: ExtractTextPlugin.extract({
			        fallback: "style-loader",
		            use: "css-loader"
		        })
			},
			{ 	test: /\.(png|woff|woff2|eot|ttf|svg)$/, 
				loader: 'url-loader?limit=100000' 
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				use: [
				  {
				    loader: 'file-loader',
				    options: {
						name: '[name].[ext]',
						outputPath: 'images/'
					}
				  }
				]
			}

			]
	}
}